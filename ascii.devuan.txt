ii  audacious                                     3.7.2-1+b1                                 amd64        small and fast audio player which supports lots of formats
ii  audacious-plugins:amd64                       3.7.2-2.1                                  amd64        Base plugins for audacious
ii  audacious-plugins-data                        3.7.2-2.1                                  all          Data files for Audacious plugins
ii  libaudcore3:amd64                             3.7.2-1+b1                                 amd64        audacious core engine library
ii  libaudgui3:amd64                              3.7.2-1+b1                                 amd64        audacious media player (libaudgui shared library)
ii  libaudqt0:amd64                               3.7.2-1+b1                                 amd64        audacious media player (libaudqt shared library)
ii  libaudtag2:amd64                              3.7.2-1+b1                                 amd64        audacious media player (libaudtag shared library)
